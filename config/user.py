#!/usr/bin/python
from configparser import ConfigParser

from .default import *

def config(archivo = "/config/conexion.ini", seccion = "postgresql"):
    # Crear el parser y leer el archivo
    parser = ConfigParser()
    parser.read(archivo)
 
    # Obtener la sección de conexión a la base de datos
    db = {}
    if parser.has_section(seccion):
        params = parser.items(seccion)
        for param in params:
            db[param[0]] = param[1]
    else:
        raise Exception('Secccion {0} no encontrada en el archivo {1}'.format(seccion, archivo))

    return "postgresql://" + db['user'] + ':' + db['password'] + '@' + db['host'] + ':' + db['port'] + '/' + db['database']

SQLALCHEMY_DATABASE_URI = config()