#!/usr/bin/python
from os import getenv

from .default import *

SQLALCHEMY_DATABASE_URI = getenv("DATABASE_URL")