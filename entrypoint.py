#!/usr/bin/python
from app import create_app
from os import getenv

settings_module = getenv('ENV')

app = create_app(settings_module)

if __name__ == "__main__":
    app.run()