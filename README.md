## LAPBOOK

Sistema de gestion de materias y notas para estudiantes y profesores

### Instalación

```bash
pip install -r requirements.txt
```

El proyecto usa como gestor de base de datos Postgresql, por lo tanto será necesario tener instalado dicho gestor.

Tambien se requiere una variable de entorno: ENV, en la que se situará la ubicación del archivo de configuracion .py

Linux o MacOS:

```bash
export ENV="config.user"
```
Windows:

```bash
set "ENV=config.user"
```

En el archivo *conexion.ini* se debe colocar la información de la base de datos a conectar

### Ejecución

```bash
gunicorn entrypoint:app
```