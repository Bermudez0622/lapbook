#!/usr/bin/python
from flask import Flask
from flask_login import LoginManager
from flask_sqlalchemy import SQLAlchemy

from .common.models import db

login_manager = LoginManager()

def create_app(settings_module='config.development'):

    app = Flask(__name__)
    app.config.from_object(settings_module)

    login_manager.init_app(app)
    login_manager.login_view = "users.log"

    db.init_app(app)
    with app.app_context():
        db.create_all()

    #Blueprints
    from .private import private_bp
    app.register_blueprint(private_bp)

    from .public import public_bp
    app.register_blueprint(public_bp)

    from .users import users_bp
    app.register_blueprint(users_bp)

    return app