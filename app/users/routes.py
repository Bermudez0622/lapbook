#!/usr/bin/python
from flask import render_template, request, redirect, url_for
from flask_login import current_user, login_user
from werkzeug.urls import url_parse

from .form import LoginForm, SignupForm
from app.common.models import User
from . import users_bp

@users_bp.route('/login', methods = ["GET", "POST"])
def log():
    if current_user.is_authenticated:
        return redirect(url_for("private.logueado"))
    form = LoginForm()
    if form.validate_on_submit():
        user = User.get_by_email(form.email.data)
        if user is not None and user.check_password(form.password.data):
            login_user(user, remember = form.remember.data)
            next_page = request.args.get('next')
            if not next_page or url_parse(next_page).netloc != '':
                next_page = url_for("private.logueado")
            return redirect(next_page)
    return render_template("log-in.html", form = form, title = "Iniciar Sesion")

@users_bp.route('/register', methods = ["GET", "POST"])
def sign():
    if current_user.is_authenticated:
        return redirect(url_for("private.logueado"))
    form = SignupForm()
    error = None
    if form.validate_on_submit():
        user = User.get_by_email(form.email.data)
        if user is not None:
            error = f"El email {form.email.data} ya está siendo utilizado por otro usuario"
        else:
            user = User(name = form.name.data, email = form.email.data, ie = form.ie.data, perfil = form.perfil.data)
            user.set_password(form.password.data)
            user.save()
            login_user(user, remember = True)
            next_page = request.args.get('next', None)
            if not next_page or url_parse(next_page).netloc != '':
                next_page = url_for("private.logueado")
            return redirect(next_page)
    return render_template("sign-up.html", form = form, error = error, title = "Registarse")