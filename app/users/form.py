#!/usr/bin/python
from flask_wtf import FlaskForm
from wtforms import StringField, SubmitField, PasswordField, SelectField, BooleanField
from wtforms.validators import DataRequired, Email

required = "Necesitas rellenar este campo"
email = "Correo electronico no valido"

class SignupForm(FlaskForm):
    name = StringField("Nombre", validators = [DataRequired(required)])
    email = StringField("Correo Electronico", validators = [DataRequired(required), Email(email)])
    ie = SelectField("Institucion Educativa", choices = [("I.E Antonio Nariño", "I.E Antonio Nariño")], validators = [DataRequired(required)])
    password = PasswordField("Contraseña", validators = [DataRequired(required)])
    perfil = SelectField("Perfil", choices = [("Profesor", "Profesor"), ("Estudiante", "Estudiante")], validators = [DataRequired(required)])
    submit = SubmitField("Registrarse")
    
class LoginForm(FlaskForm):
    email = StringField("Correo Electronico", validators = [DataRequired(required), Email(email)])
    password = PasswordField("Contraseña", validators = [DataRequired(required)])
    remember = BooleanField("Recuérdame")
    submit = SubmitField("Iniciar Sesion")