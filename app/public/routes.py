#!/usr/bin/python
from flask import render_template, redirect, url_for
from flask_login import current_user

from . import public_bp

@public_bp.route('/')
def index():
    if current_user.is_authenticated:
        return redirect(url_for("private.logueado"))
    return render_template("index.html", title = "Inicio")