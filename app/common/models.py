#!/usr/bin/python
from flask_login import UserMixin
from werkzeug.security import generate_password_hash, check_password_hash
from flask_sqlalchemy import SQLAlchemy

db = SQLAlchemy()

class Ie(db.Model):
    __tablename__ = "instituciones"
    #Atributos
    id = db.Column(db.Integer, primary_key = True)
    name = db.Column(db.String, nullable = False)
    #Metodos
    def save(self):
        if not self.id:
            db.session.add(self)
        db.session.commit()
    @staticmethod
    def get_by_id(id):
        return Ie.query.get(id)
    @staticmethod
    def get_by_name(name):
        return Ie.query.filter_by(name = name).first()

class Perfil(db.Model):
    __tablename__ = "perfil"
    #Atributos
    id = db.Column(db.Integer, primary_key = True)
    name = db.Column(db.String, nullable = False)
    #Metodos
    def save(self):
        if not self.id:
            db.session.add(self)
        db.session.commit()
    @staticmethod
    def get_by_id(id):
        return Perfil.query.get(id)
    @staticmethod
    def get_by_name(name):
        return Perfil.query.filter_by(name = name).first()

class User(db.Model, UserMixin):
    __tablename__ = "usuarios"
    #Atributos
    id = db.Column(db.Integer, primary_key = True)
    name = db.Column(db.String, nullable = False)
    email = db.Column(db.String, unique = True, nullable = False)
    ie = db.Column(db.Integer, db.ForeignKey("instituciones.id", ondelete = "CASCADE"), nullable = False)
    password = db.Column(db.String, nullable = False)
    perfil = db.Column(db.Integer, db.ForeignKey("perfil.id", ondelete = "CASCADE"), nullable = False)
    is_admin = db.Column(db.Boolean, default = False)
    #Metodos
    def __repr__(self):
        return f"<user {self.email}>"
    def set_password(self, password):
        self.password = generate_password_hash(password)
    def check_password(self, password):
        return check_password_hash(self.password, password)
    def save(self):
        if not self.id:
            db.session.add(self)
        db.session.commit()
    @staticmethod
    def get_by_id(id):
        return User.query.get(id)
    @staticmethod
    def get_by_email(email):
        return User.query.filter_by(email = email).first()