#!/usr/bin/python
from flask import Blueprint

private_bp = Blueprint("private", __name__, template_folder = "templates/private", static_folder = "static/private")

from . import routes