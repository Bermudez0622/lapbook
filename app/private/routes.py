#!/usr/bin/python
from flask import render_template, redirect, url_for
from flask_login import logout_user, login_required

from . import private_bp
from app import login_manager
from app.common.models import User

@private_bp.route("/logout")
def logout():
    logout_user()
    return redirect(url_for("public.index"))

@private_bp.route("/usuario")
@login_required
def logueado():
    return render_template("students.html")

@login_manager.user_loader
def load_user(user_id):
    return User.get_by_id(int(user_id))